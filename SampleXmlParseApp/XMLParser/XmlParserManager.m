//
//  XmlParserManager.m
//  SampleXmlParseApp
//
//  Created by ALTEN India on 31/01/19.
//  Copyright © 2019 ALTEN India. All rights reserved.
//

#import "XmlParserManager.h"
#import "AirConditionSettingModel.h"

@interface XmlParserManager() <NSXMLParserDelegate>

@property(nonatomic, retain) NSMutableArray<AircraftFamilyModel *> *aircraftFamilyArrayMutable;
@property(nonatomic, retain) NSXMLParser *xmlParser;
@property(nonatomic, retain) NSMutableString *stringValue;
@property(nonatomic, retain) AirConditionSettingModel *airConditionSettingObj;
@property(nonatomic, retain) AircraftFamilyModel *aircraftFamilyObj;
@property(nonatomic, retain) AntiIceModel *antiIceObj;
@property(nonatomic, retain) NSString *currentTagName;
@property(nonatomic, retain) NSString *currentAntiIceTag;

@end

@implementation XmlParserManager

@synthesize xmlParser,stringValue;

@synthesize airConditionSettingObj,aircraftFamilyObj,antiIceObj,currentAntiIceTag;

-(id)initWithData:(NSData *)data {
    
    if ( self = [super init] ) {
        
        self.xmlParser=[[NSXMLParser alloc] initWithData:data];
        
        self.xmlParser.delegate=self;
        if([self.xmlParser parse]){
            //If parsing completed successfully
            
            NSLog(@"No Errors");
            
        }else {
            NSLog(@"Error Error Error!!!");
            
        }
    }
    return self;
}
#pragma mark - Public methods

-(NSArray<AircraftFamilyModel *> *) aircraftFamilyArray {
    return self.aircraftFamilyArrayMutable;
}

-(AircraftFamilyModel * _Nullable) getInfosForAircraftFamily:(NSString *)aircraftFamily {
    NSPredicate *familyPredicate = [NSPredicate predicateWithFormat:@"SELF.aircraftFamilyName LIKE %@", aircraftFamily];
    NSArray *filtered = [self.aircraftFamilyArrayMutable filteredArrayUsingPredicate:familyPredicate];
    return [filtered firstObject];
}

#pragma mark - Parsing XML
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary<NSString *,NSString *> *)attributeDict
{
    if([elementName isEqualToString:@"AirConditionningLabels"]) {
        //Initialize the array.
        self.aircraftFamilyArrayMutable = [[NSMutableArray alloc] init];
        
    }
    else if([elementName isEqualToString:@"AircraftFamily"]) {
        
        aircraftFamilyObj = [[AircraftFamilyModel alloc] init];
        aircraftFamilyObj.aircraftFamilyName = [attributeDict objectForKey:@"aircraftFamilyName"];
        aircraftFamilyObj.antiIceArray = [[NSMutableArray alloc] init];
        
    }else if([elementName isEqualToString:@"ANTIICE"]) {
        
        self.currentAntiIceTag = @"antiIce";
        self.antiIceObj = [[AntiIceModel alloc] init];
        self.antiIceObj.valueName = [attributeDict objectForKey:@"value"];
        
    }
    else if([elementName isEqualToString:@"AEO"]) {
        if ([self.currentAntiIceTag isEqualToString:@"antiIce"]) {
            self.currentTagName = elementName;
            antiIceObj.AEOModelObject.airConditionningSettingsArray = [[NSMutableArray alloc] init];
        }else {
            self.currentTagName = elementName;
            aircraftFamilyObj.AEOModelObject.airConditionningSettingsArray = [[NSMutableArray alloc] init];
        }
        
    }else if([elementName isEqualToString:@"OEI"]) {
        if ([self.currentAntiIceTag isEqualToString:@"antiIce"]) {
            self.currentTagName = elementName;
            antiIceObj.OEIModelObject.airConditionningSettingsArray = [[NSMutableArray alloc] init];
        }else {
            self.currentTagName = elementName;
            aircraftFamilyObj.OEIModelObject.airConditionningSettingsArray = [[NSMutableArray alloc] init];
        }
    }else if([elementName isEqualToString:@"TEI"]) {
        if ([self.currentAntiIceTag isEqualToString:@"antiIce"]) {
            self.currentTagName = elementName;
            antiIceObj.TEIModelObject.airConditionningSettingsArray = [[NSMutableArray alloc] init];
        }else {
            self.currentTagName = elementName;
            aircraftFamilyObj.TEIModelObject.airConditionningSettingsArray = [[NSMutableArray alloc] init];
        }
    }else if([elementName isEqualToString:@"TREI"]) {
        
        if ([self.currentAntiIceTag isEqualToString:@"antiIce"]) {
            self.currentTagName = elementName;
            antiIceObj.TREIModelObject.airConditionningSettingsArray = [[NSMutableArray alloc] init];
        }else {
            self.currentTagName = elementName;
            aircraftFamilyObj.TREIModelObject.airConditionningSettingsArray = [[NSMutableArray alloc] init];
        }
    }else if([elementName isEqualToString:@"AEI"]) {
        if ([self.currentAntiIceTag isEqualToString:@"antiIce"]) {
            self.currentTagName = elementName;
            antiIceObj.AEIModelObject.airConditionningSettingsArray = [[NSMutableArray alloc] init];
        }else {
            self.currentTagName = elementName;
            aircraftFamilyObj.AEIModelObject.airConditionningSettingsArray = [[NSMutableArray alloc] init];
        }
    }
    else if([elementName isEqualToString:@"AirConditionningSetting"]) {
        if (self.airConditionSettingObj == nil) {
            self.airConditionSettingObj = [[AirConditionSettingModel alloc] init];
            self.airConditionSettingObj.identifier = [attributeDict objectForKey:@"identifier"];
        }
        
    }
    
}
-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if(!stringValue)
        stringValue = [[NSMutableString alloc] initWithString:string];
    else
        [stringValue appendString:[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    
    NSLog(@"Value: %@", stringValue);
    
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    
    if([elementName isEqualToString:@"AirConditionningLabels"])
        return;
    
    if ([elementName isEqualToString:@"AircraftFamily"]) {
        
        [self.aircraftFamilyArrayMutable addObject:aircraftFamilyObj];
        aircraftFamilyObj = nil;
    }
    if ([elementName isEqualToString:@"ANTIICE"]) {
        
        [self.aircraftFamilyObj.antiIceArray addObject:antiIceObj];
        antiIceObj = nil;
    }
    
    if([elementName isEqualToString:@"Label"]) {
        
        self.airConditionSettingObj.label = stringValue;
        
        stringValue = nil;
        
    }else if([elementName isEqualToString:@"Inerting"]) {
        
        self.airConditionSettingObj.inerting = stringValue;
        stringValue = nil;
        
        
    }else if([elementName isEqualToString:@"Oboggs"]) {
        
        self.airConditionSettingObj.oboggs = stringValue;
        stringValue = nil;
        
        
    }else {
        
    }
    
    if([elementName isEqualToString:@"AirConditionningSetting"]) {
        if([self.currentTagName isEqualToString:@"AEO"])
        {
            if ([self.currentAntiIceTag isEqualToString:@"antiIce"]) {
                [antiIceObj.AEOModelObject.airConditionningSettingsArray  addObject:self.airConditionSettingObj];
                airConditionSettingObj = nil;
                
            }else {
                [aircraftFamilyObj.AEOModelObject.airConditionningSettingsArray  addObject:self.airConditionSettingObj];
                airConditionSettingObj = nil;
                
            }
        }else if([self.currentTagName isEqualToString:@"AEI"])
        {
            if ([self.currentAntiIceTag isEqualToString:@"antiIce"]) {
                [antiIceObj.AEIModelObject.airConditionningSettingsArray  addObject:self.airConditionSettingObj];
                airConditionSettingObj = nil;
                
            }else {
                [aircraftFamilyObj.AEIModelObject.airConditionningSettingsArray  addObject:self.airConditionSettingObj];
                airConditionSettingObj = nil;
            }
        }
        else if([self.currentTagName isEqualToString:@"OEI"])
        {
            if ([self.currentAntiIceTag isEqualToString:@"antiIce"]) {
                [antiIceObj.OEIModelObject.airConditionningSettingsArray  addObject:self.airConditionSettingObj];
                airConditionSettingObj = nil;
                
            }else {
                [aircraftFamilyObj.OEIModelObject.airConditionningSettingsArray  addObject:self.airConditionSettingObj];
                airConditionSettingObj = nil;
            }
        }
        else if([self.currentTagName isEqualToString:@"TEI"])
        {
            if ([self.currentAntiIceTag isEqualToString:@"antiIce"]) {
                [antiIceObj.TEIModelObject.airConditionningSettingsArray  addObject:self.airConditionSettingObj];
                airConditionSettingObj = nil;
                
            }else {
                [aircraftFamilyObj.TEIModelObject.airConditionningSettingsArray  addObject:self.airConditionSettingObj];
                airConditionSettingObj = nil;
            }
        }
        else if([self.currentTagName isEqualToString:@"TREI"])
        {
            if ([self.currentAntiIceTag isEqualToString:@"antiIce"]) {
                [antiIceObj.TREIModelObject.airConditionningSettingsArray  addObject:self.airConditionSettingObj];
                airConditionSettingObj = nil;
                
            }else {
                [aircraftFamilyObj.TREIModelObject.airConditionningSettingsArray  addObject:self.airConditionSettingObj];
                airConditionSettingObj = nil;
            }
            
        }
        
    }
    
}

@end
