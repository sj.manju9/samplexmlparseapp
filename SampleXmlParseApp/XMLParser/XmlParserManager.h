//
//  XmlParserManager.h
//  SampleXmlParseApp
//
//  Created by ALTEN India on 31/01/19.
//  Copyright © 2019 ALTEN India. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AircraftFamilyModel.h"
#import "AntiIceModel.h"

#import "AirConditionSettingModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XmlParserManager : NSObject <NSXMLParserDelegate>

@property(nonatomic, readonly) NSArray<AircraftFamilyModel *> *aircraftFamilyArray;

-(id)initWithData:(NSData *)data;
-(AircraftFamilyModel * _Nullable) getInfosForAircraftFamily:(NSString *)aircraftFamily;

@end

NS_ASSUME_NONNULL_END
