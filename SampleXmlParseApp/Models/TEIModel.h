//
//  TEIModel.h
//  SampleXmlParseApp
//
//  Created by ALTEN India on 31/01/19.
//  Copyright © 2019 ALTEN India. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TEIModel : NSObject

@property(nonatomic,retain) NSMutableArray *airConditionningSettingsArray;

@end

NS_ASSUME_NONNULL_END
