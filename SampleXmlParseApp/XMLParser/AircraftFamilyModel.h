//
//  AircraftFamilyModel.h
//  SampleXmlParseApp
//
//  Created by ALTEN India on 31/01/19.
//  Copyright © 2019 ALTEN India. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AEOModel.h"
#import "OEIModel.h"
#import "AEIModel.h"
#import "TEIModel.h"
#import "TREIModel.h"
#import "AEIModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AircraftFamilyModel : NSObject

@property(nonatomic,retain) NSString *aircraftFamilyName;

@property(nonatomic,retain) NSMutableArray *antiIceArray;

@property(nonatomic,retain) AEOModel *AEOModelObject;
@property(nonatomic,retain) AEIModel *AEIModelObject;

@property(nonatomic,retain) OEIModel *OEIModelObject;
@property(nonatomic,retain) TEIModel *TEIModelObject;
@property(nonatomic,retain) TREIModel *TREIModelObject;


@end

NS_ASSUME_NONNULL_END
