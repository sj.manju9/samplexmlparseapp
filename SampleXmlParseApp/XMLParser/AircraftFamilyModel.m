//
//  AircraftFamilyModel.m
//  SampleXmlParseApp
//
//  Created by ALTEN India on 31/01/19.
//  Copyright © 2019 ALTEN India. All rights reserved.
//

#import "AircraftFamilyModel.h"

@implementation AircraftFamilyModel

@synthesize antiIceArray,aircraftFamilyName;

@synthesize AEOModelObject,AEIModelObject,TEIModelObject,TREIModelObject,OEIModelObject;

-(id) init{
    if ( self = [super init] ) {
        
        self.AEOModelObject = [[AEOModel alloc] init];
        self.AEIModelObject = [[AEIModel alloc] init];
        self.OEIModelObject = [[OEIModel alloc] init];
        self.TEIModelObject = [[TEIModel alloc] init];

        self.TREIModelObject = [[TREIModel alloc] init];

    
    }
    return self;
}
@end
